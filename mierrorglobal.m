function [error,ev] = mierrorglobal(met,f1,f2,intv,y0,N)

[t,y,ev] = met(f1,intv,y0,N);

%error = norm(f2(t)-y,2);
error = max(max(abs(f2(t) - y)));