function[T,Y,j] = mieulermod(f, intv, y0, N)

a = min(intv);
b = max(intv);
h = (b - a)/N;
 
t = a;
y = y0;
j = 0;

T = t;
Y = y;

for k=1:N
    
    j = j + 2;

    l1 = f(t,y);
    l2 = f( t + h/2, y + (h/2) * l1);

    t = t + h;
    y = y + h * l2;

    T = [T,t];
    Y = [Y,y];
end

end