intv = [0 10];
x0 = [2; 3];
N0 = 100;
f1 = @(t,y) [-2*y(1)+ y(2) + 2*sin(t) ; y(1) - 2*y(2) + 2*(cos(t)-sin(t))];
f2 = @(t) [2*exp(-t) + sin(t); 2*exp(-t) + cos(t)];

[t,y] = mieuler(f1,intv,x0,100);


error = max(max(abs(f2(t) - y)));
disp(error);