function [error, eVal] = globalError(met,f1,f2,intv,x0,N)

[t,y,eVal] = met(f1,intv,x0,N);

error = max(max(abs(f2(t) - y)));

