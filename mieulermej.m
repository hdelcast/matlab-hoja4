function[T,Y,j] = mieulermej(f, intv, y0, N)
 
a = min(intv);
b = max(intv);
h = (b - a)/N;

t = a;
y = y0;
j = 0;

T = [];
Y = [];

T= t;
Y = y;

for k=1:N

    j = j + 2;
    
    s1 = f(t,y);
    yE = y + s1*h;
    s2 = f(t+h, yE);

    t = t + h;
    y = y + (h/2) * (s1 + s2);

    T = [T, t];
    Y = [Y, y];
end
end
